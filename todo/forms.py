from django import forms
from . import models

class Formdua(forms.ModelForm):
    tanggal=forms.DateField(widget=forms.DateInput(
        attrs={
            "type":"date"
        }
    ))
    # hari=forms.CharField(widget=forms.TextInput(
    #     attrs={
    #         "placeholder":"Please input hari"
    #     }
    # ))
    jam=forms.TimeField(widget=forms.TimeInput(
        attrs={
            "type":"time",
            "class":"form-control"
        }
    ))

    kegiatan=forms.CharField(widget=forms.TextInput(
        attrs={
            "placeholder":"Please input kegiatan"
        }
    ))
    tempat=forms.CharField(widget=forms.TextInput(
        attrs={
            "placeholder":"Please input tempat"
        }
    ))
    kategori=forms.CharField(widget=forms.TextInput(
        attrs={
            "placeholder":"Please input kategori"
        }
    ))

    class Meta:
        model=models.Todo
        fields=['tanggal','kegiatan','tempat','kategori']
