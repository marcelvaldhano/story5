# Generated by Django 2.2.6 on 2019-10-08 03:08

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0011_auto_20191008_0817'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todo',
            name='jam',
            field=models.TimeField(default=datetime.datetime(2019, 10, 8, 3, 8, 30, 784105, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='todo',
            name='tanggal',
            field=models.DateField(default=datetime.datetime(2019, 10, 8, 3, 8, 30, 784105, tzinfo=utc)),
        ),
    ]
