# Generated by Django 2.2.6 on 2019-10-07 15:35

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0004_auto_20191007_2146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todo',
            name='jam',
            field=models.TimeField(default=datetime.datetime(2019, 10, 7, 15, 35, 30, 7700, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='todo',
            name='tanggal',
            field=models.DateField(default=datetime.datetime(2019, 10, 7, 15, 35, 30, 7700, tzinfo=utc)),
        ),
    ]
