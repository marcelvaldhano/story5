from django.db import models
from django.utils import timezone
# Create your models here.
class Todo(models.Model):
    hari=models.CharField(max_length=100, default="")
    tanggal=models.DateField(blank=False, default= timezone.now())
    jam=models.TimeField(blank=False, default=timezone.now())
    kegiatan=models.CharField(max_length=100, default="")
    tempat=models.CharField(max_length=100,default="")
    kategori=models.CharField(max_length=100,default="")

 