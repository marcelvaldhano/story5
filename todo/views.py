from django.shortcuts import render, redirect
from .forms import Formdua
from .models import Todo
from . import forms
from datetime import datetime,date
# from .forms import TodoForm, Formdua

# Create your views here.
def utama(request):
    context={}
    return render(request,'todo/Halaman-utama.html', context)

def biodata(request):
    context={}
    return render(request,'todo/Halaman-biodata.html', context)

def list(request):
    if request.method =='POST':
        form=Formdua(request.POST)
        if form.is_valid():
            tanggal=form.cleaned_data['tanggal']
            hari=tanggal.strftime("%A")
            jam=form.cleaned_data['jam']
            kegiatan=form.cleaned_data['kegiatan']
            tempat=form.cleaned_data['tempat']
            kategori=form.cleaned_data['kategori']

            todo=Todo(hari=hari,tanggal=tanggal,jam=jam,kegiatan=kegiatan,tempat=tempat,kategori=kategori)
            todo.save()
            return redirect ('list')
        else:
            return render(request,'todo/list.html',{'form':form})
    list2 = forms.Formdua()
    list3 = Todo.objects.all()
    return render(request,'todo/list.html',{'form':list2, 'todo':list3})

def removelist(request,list_id):
    if Todo.objects.filter(id=list_id).exists():
        Todo.objects.get(id=list_id).delete()
    return redirect('list')
# def formdua_detail(request):
#     if request.method =='POST':
#         form=Formdua(request.POST)
#         if form.is_valid():
#             form.save()

#     list=forms.Formdua()
#     return render(request,'todo/list.html',{'form':list})
