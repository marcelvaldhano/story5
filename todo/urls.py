from django.urls import path
from . import views

urlpatterns = [
    path('',views.utama, name='utama'),
    path('biodata',views.biodata, name='biodata'),
    path('list',views.list, name='list'),
    path('delete/<int:list_id>/',views.removelist, name='delete'),
]